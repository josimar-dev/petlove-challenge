const express = require("express");
const cors = require("cors");
const axios = require("axios");
const app = express();

app.use(cors());

app.get("/api/:cep", async (req, res) => {
  try {
    const normalizedData = {};
    const response = await axios.get(`https://viacep.com.br/ws/${req.params.cep}/json/`);
    const data = response.data;
    console.log("RESPONSE: ", data);
    normalizedData.cep = data.cep;
    normalizedData.logradouro = data.logradouro;
    normalizedData.localidade = data.localidade;
    normalizedData.uf = data.uf;
    res.send(normalizedData);
  } catch (error) {
    res.status(406).send('CEP inválido...');
  }
});

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
