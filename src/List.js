import React from "react";

function List(prop) {
  const { result, isActive } = prop;
  if (isActive) {
    return(
      <ul className="form__list">
        <li>
          <b>CEP:</b>
          <span>{` ${result.cep}`}</span>
        </li>
        <li>
          <b>Estado:</b>
          <span>{` ${result.uf}`}</span>
        </li>
        <li>
          <b>Cidade:</b>
          <span>{` ${result.localidade}`}</span>
        </li>
        <li>
          <b>Logradouro:</b>
          <span>{` ${result.logradouro}`}</span>
        </li>
      </ul>
    );
  }
  return null;
}

export default List;