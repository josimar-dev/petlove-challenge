import React from 'react';

function Input(props) {
  const { onChange } = props;
  return (
    <input
      type="text"
      className="form__input"
      placeholder="00000-000"
      onChange={onChange}
      pattern="^[0-9]{8}$"
      maxLength="8"
      required
      title="Insira um CEP válido"
    />
  );
}

export default Input;