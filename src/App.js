import React, { useState, useEffect } from "react";
import { hot } from "react-hot-loader";
import axios from "axios";
import "./App.css";
import "./Input";
import Input from "./Input";
import List from "./List";

function App() {
  const [status, setStatus] = useState(false);
  const [value, setValue] = useState("");
  const [listData, setList] = useState(null);
  const [invalid, setInvalid] = useState(false);

  function getCep(e) {
    const inputValue = e.currentTarget.value;
    setValue(inputValue);
  }

  function sendCep(e) {
    e.preventDefault();
    const response = axios.get(`http://localhost:5000/api/${value}`)
      .then((res) => {
        setStatus(false);
        setInvalid(true);
        setList(res.data);
        if (res.data && res.data.cep) {
          setStatus(true);
          setInvalid(false);
        }
      })
      .catch((e) => {
        setStatus(false);
        setInvalid(true);
      });
  }

  return (
    <div className="background">
      <form className="form" onSubmit={sendCep}>
        <Input onChange={getCep} />
        <input type="submit" value="Buscar CEP" className="form__button" />
      </form>
      { invalid ? <span className="error-message">Insira um CEP válido.</span> : null }
      <List result={listData} isActive={status} />
    </div>
  );
}

export default hot(module)(App);