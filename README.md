# Petlove Challenge


## Getting Started

The first thing you have to do is install the required modules the app needs.
In your terminal, go to the project folder.
Once you're there, just type the following command:

```
npm install
```

## Running the Servers

Now that you have all the modules installed, you need to start the servers.

### Server Side

In your terminal, go to the "server" folder. After doing that, just type the following
command:

```
node index.js
```

Now you have an express server running, which is the one that provides the data that
the client relies upon.

### Client Side
In your terminal, go to the root folder of the app and type the following:

```
npm start
```

Now, you have both servers running, everything you need to run
the application properly has been successfully set up.

## Authors

* **Josimar Bezerra**


